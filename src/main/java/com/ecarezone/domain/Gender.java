package com.ecarezone.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GENDER")
public class Gender {
	@Id
	@Column(name = "ID")
	public Long id;
	
	@Column(name = "DESCRIPTION")
	public String name;

	
	
}
