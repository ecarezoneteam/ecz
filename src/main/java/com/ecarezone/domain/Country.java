package com.ecarezone.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COUNTRY")
public class Country {
	
	@Id
	@Column(name = "ID")
	public Long id;
	
	@Column(name = "NAME")
	public String name;

	
}
