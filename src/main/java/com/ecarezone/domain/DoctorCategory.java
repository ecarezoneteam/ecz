package com.ecarezone.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DOCTOR_CATEGORY")
public class DoctorCategory {
	@Id
	@Column(name = "ID")
	public Long id;
	
	@Column(name = "CATEGORY")
	public String name;

	
	
}
