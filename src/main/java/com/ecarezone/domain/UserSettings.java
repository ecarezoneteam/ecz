package com.ecarezone.domain;

/**
 * Created by jifeng on 22/04/15.
 */
public class UserSettings {
    String email;
    String country;
    String language;

    public UserSettings(String email, String country, String language) {
        this.email = email;
        this.country = country;
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public String getCountry() {
        return country;
    }

    public String getLanguage() {
        return language;
    }
}
