package com.ecarezone.domain;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public class User {
	@Column(name = "PASSWORD")
	private String hashedPassword;
	
	@Column(name = "EMAIL")
	private String email;
	
	@JoinColumn(name = "COUNTRY_ID")
	@OneToOne
	private Country country;
	
	@JoinColumn(name = "LANGUAGE_ID")
	@OneToOne
	private Language language;
	
	@Column(name = "NAME")
	private String name;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Country getCountry() {
		return country;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}

/*	public Country getCountryByName(String countryName) {
		return Country.findByName(countryName);
	}*/

	
	public Language getLanguage() {
		return language;
	}
	
	public void setLanguage(Language language) {
		this.language = language;
	}

	/*public Language getLanguageByName(String languageName) {
		return Language.findByName(languageName);
	}*/

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
