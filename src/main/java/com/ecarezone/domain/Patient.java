package com.ecarezone.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.swing.SpringLayout.Constraints;

@Entity(name = "PATIENT")
@Table(name = "PATIENT")
public class Patient extends User implements Serializable{
	
	@Id
	@Column(name = "ID")
    private Long userId;
	
	@OneToMany (mappedBy = "patient")
	private List<PatientProfile> profiles;
	
	@Column(name = "RECOMMANDED_DOCTOR_ID")
	private Long recommandedDoctorId;
    
    @Column(name = "STATUS")
    private String status;
    
    @Column(name = "IS_CALL_ALLOWED")
    private String isCallAllowed;

/*	private UserSettings userSettings;
    private UserProfile userProfile;*/
	
    public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<PatientProfile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<PatientProfile> profiles) {
		this.profiles = profiles;
	}
    
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsCallAllowed() {
		return isCallAllowed;
	}

	public void setIsCallAllowed(String isCallAllowed) {
		this.isCallAllowed = isCallAllowed;
	}

	public Long getRecommandedDoctorId() {
		return recommandedDoctorId;
	}

	public void setRecommandedDoctorId(Long recommandedDoctorId) {
		this.recommandedDoctorId = recommandedDoctorId;
	}

	public Long getUserId() {
		return userId;
	}
	
}
