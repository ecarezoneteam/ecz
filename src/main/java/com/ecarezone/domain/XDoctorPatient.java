package com.ecarezone.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "X_DOCTOR_PATIENT")
public class XDoctorPatient {
	
	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "DOCTOR_ID")
	private Long doctorId;
	
	@Column(name = "PATIENT_ID")
	private Long patientId;

	@Column(name = "STATUS")
	private String status;
	
/*	@Column(name = "REQUESTEDDATE")
	private Date requestedDate;*/


	public XDoctorPatient() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/*public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	*/
}
