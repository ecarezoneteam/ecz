package com.ecarezone.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by jifeng on 22/04/15.
 */
public class UserProfile {
    String email;
    String gender;
    Integer height;

    public UserProfile(String email, String gender, Integer height) {
        this.email = email;
        this.gender = gender;
        this.height = height;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public Integer getHeight() {
        return height;
    }
}
