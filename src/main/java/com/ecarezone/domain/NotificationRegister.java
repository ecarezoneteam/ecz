package com.ecarezone.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NOTIFICATION_REGISTER")
public class NotificationRegister {
	
	@Id
	@Column(name = "ID")
    private Long Id;
	
	@Column(name = "EMAIL")
	private String email;
    
	@Column(name = "DEVICE_UNIQUE")
	private String deviceUnique;
	
	@Column(name = "OS")
	private String os;
	
	@Column(name = "ROLE")
	private String role;
	
	@Column(name = "STATUS")
	private String status;
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeviceUnique() {
		return deviceUnique;
	}

	public void setDeviceUnique(String deviceUnique) {
		this.deviceUnique = deviceUnique;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
