package com.ecarezone.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PATIENT_PROFILE")
public class PatientProfile {
	
	@Id
	@Column(name = "ID")
	public Long id;
	
	@ManyToOne (fetch=FetchType.LAZY)
	@JoinColumn(name = "PATIENT_ID")
	public Patient patient;
	
	@Column(name = "PROFILE_NAME")
	public String profileName;
	
	@Column(name = "EMAIL")
	public String email;
	
/*	@Column(name = "GENDER_ID")
	@OneToOne
	public Gender gender;*/
	
	@Column(name = "GENDER_ID")
	private Long genderId;
	
	@Column(name = "HEIGHT")
	public Integer height;
	
	@Column(name = "NAME")
	public String name;
	
	@Column(name = "ADDRESS")
	public String address;
	
	@Column(name = "BIRTH_DATE")
	public Date birthdate;
	
	@Column(name = "WEIGHT")
	public Integer weight;

	@Column(name = "AVATAR_URL")
	public String avatarUrl;
	
	@Column(name = "ETHNICITY")
	public String ethnicity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/*public String getGender() {
		return gender.name;
	}

	public void setGender(String name) {
		this.gender = Gender.findByName(name);
	}*/

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public PatientProfile(Long id, Patient patient, String profileName,
			String email, Long genderId, Integer height, String name,
			String address, Date birthdate, Integer weight, String ethnicity) {
		super();
		this.id = id;
		this.patient = patient;
		this.profileName = profileName;
		this.email = email;
		this.genderId = genderId;
		this.height = height;
		this.name = name;
		this.address = address;
		this.birthdate = birthdate;
		this.weight = weight;
		this.ethnicity = ethnicity;
	}
	
}
