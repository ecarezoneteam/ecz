package com.ecarezone.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DOCTOR")
public class Doctor extends User{
	
	@Id
	@Column(name = "ID")
    private Long doctorId;
	
	@JoinColumn(name = "CATEGORY_ID")
	@OneToOne
    private DoctorCategory doctorCategory;
	
	@JoinColumn(name = "GENDER_ID")
	@OneToOne
    private Gender doctorGender;
	
	@Column(name = "DESCRIPTION")
    private String doctorDescription;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "IS_CALL_ALLOWED")
	private String isCallAllowed;
	
	@Column(name = "AVATAR_URL")
	private String avatarUrl;

   /* public Doctor(Long doctorId, DoctorCategory doctorCategory, Gender doctorGender, Country doctorCountry, Language doctorLanguage, 
    		String doctorDescription, String status, String isCallAllowed) {
        this.doctorId = doctorId;
        this.doctorCategory = doctorCategory;
        this.doctorGender = doctorGender;
        setCountry(doctorCountry);
        setLanguage(doctorLanguage);
        this.doctorDescription = doctorDescription;
        this.status = status;
        this.isCallAllowed =isCallAllowed;
    }*/

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public DoctorCategory getDoctorCategory() {
		return doctorCategory;
	}

	public void setDoctorCategory(DoctorCategory doctorCategory) {
		this.doctorCategory = doctorCategory;
	}

	public Gender getDoctorGender() {
		return doctorGender;
	}

	public void setDoctorGender(Gender doctorGender) {
		this.doctorGender = doctorGender;
	}

	public String getDoctorDescription() {
		return doctorDescription;
	}

	public void setDoctorDescription(String doctorDescription) {
		this.doctorDescription = doctorDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsCallAllowed() {
		return isCallAllowed;
	}

	public void setIsCallAllowed(String isCallAllowed) {
		this.isCallAllowed = isCallAllowed;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
}
