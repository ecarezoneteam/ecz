/**
 * 
 */
package com.ecarezone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecarezone.constants.UrbanAirshipConstants;
import com.ecarezone.dao.NotificationRegisterDAO;

@Service("NewsPush")
@Transactional
public class NewsPushService {
	
	@Autowired
	NotificationRegisterDAO notificationRegisterDAO;

	@SuppressWarnings("deprecation")
	public void pushNewsToPatientDevices(String notification) {
		
		String patientRole = "1";
		List<String> deviceUniqueKeys = notificationRegisterDAO.getPatientDeviceUnqiueKeys(patientRole);
		String appKey = UrbanAirshipConstants.APP_KEY;
		String appMasterSecret = UrbanAirshipConstants.APP_MASTER_SECRET;
		
		//String notification = "News,Fitness Plan"; //it is static
		if(!deviceUniqueKeys.isEmpty()){
			NotificationService.sendPush(appKey, appMasterSecret, deviceUniqueKeys, notification);
		}
		
	}
	
}
