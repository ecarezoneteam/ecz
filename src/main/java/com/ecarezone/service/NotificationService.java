package com.ecarezone.service;

import java.io.IOException;
import java.util.List;

import com.urbanairship.api.client.APIClient;
import com.urbanairship.api.client.APIError;
import com.urbanairship.api.client.APIErrorDetails;
import com.urbanairship.api.client.APIRequestException;
import com.urbanairship.api.client.model.APIClientResponse;
import com.urbanairship.api.client.model.APIPushResponse;
import com.urbanairship.api.push.model.DeviceType;
import com.urbanairship.api.push.model.DeviceTypeData;
import com.urbanairship.api.push.model.PushPayload;
import com.urbanairship.api.push.model.audience.Selectors;
import com.urbanairship.api.push.model.notification.Notifications;



public class NotificationService {
	
	   public static void sendPush(String appKey, String appMasterSecret, List<String> appIds, String message){


	       /* System.out.println(String.format("Make sure key and secret are set Key:%s Secret:%s",
	                                   appKey, appMasterSecret));*/

	        APIClient apiClient = APIClient.newBuilder()
	                                       .setKey(appKey)
	                                       .setSecret(appMasterSecret)
	                                       .build();
	       // System.out.println(String.format("Setup an APIClient to handle the API call %s", apiClient.toString()));
	        
			PushPayload payload = PushPayload.newBuilder()
	                                        .setAudience(Selectors.apids(appIds))
											// .setAudience(Selectors.apids("b5d4c425-a305-4363-8d6a-f3fb65635abf"))
	                                         .setNotification(Notifications.notification(message))
	                                         .setDeviceTypes(DeviceTypeData.of(DeviceType.ANDROID))
	                                         .build();

	     /*  for (String string : appIds) {
			System.out.println("app ids:"+string);
	       }*/
			try {
	            APIClientResponse<APIPushResponse> response = apiClient.push(payload);
	            System.out.println("PUSH SUCCEEDED");
	           // System.out.println(String.format("RESPONSE:%s", response.toString()));
	        }
	        catch (APIRequestException ex){
	            System.out.println(String.format("APIRequestException " + ex));
	            ex.printStackTrace();
	            System.out.println("EXCEPTION " + ex.toString());

	            APIError apiError = ex.getError().get();
	            System.out.println("Error " + apiError.getError());
	            if (apiError.getDetails().isPresent())     {
	                APIErrorDetails apiErrorDetails = apiError.getDetails().get();
	                System.out.println("Error details " + apiErrorDetails.getError());
	            }
	        }
	        catch (IOException e){
	            System.out.println("IOException in API request " + e.getMessage());
	        }

	    }
	   
	   public static void main(String[] args) {
		   String appKey = "IFWmeJ4dR_Ot8gUl0s0SdQ";
			String appMasterSecret = "mEX5kUmsSMS6wkOxS_Cy3w";
			
		sendPush(appKey, appMasterSecret, null, "Pushing Test Message from backend");
	}

}
