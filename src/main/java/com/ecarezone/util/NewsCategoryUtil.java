package com.ecarezone.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsCategoryUtil {
	
	List newsCategoryList = new ArrayList();
	
	static Map newsCategoryMap = new HashMap();
	
	public static void loadNewsCategory(){
		newsCategoryMap.put("1", "Fitness&Exercise");
		newsCategoryMap.put("2", "Men's Health");
		newsCategoryMap.put("3", "Women's Health");
		newsCategoryMap.put("4", "Healthy Eating");
		newsCategoryMap.put("5", "Parenting & Family");
		newsCategoryMap.put("6", "Other");
	}
	
	public static String getValue(String key){
		if(newsCategoryMap.size() ==0){
			loadNewsCategory();
		}
		
		if(newsCategoryMap.containsKey(key))
			return newsCategoryMap.get(key).toString();
		else
			return null;
	}
	

}
