package com.ecarezone.util;

import java.util.Date;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public class HashUtil {
	public static String uniqueAvatarId(String fileName){
        return new String(Hex.encodeHex(DigestUtils.md5(fileName + new Date().toString())));
    }

}
