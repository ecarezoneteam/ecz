/**
 * 
 */
package com.ecarezone.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;

import com.ecarezone.dao.DoctorDAO;
import com.ecarezone.dao.PatientDAO;
import com.ecarezone.domain.Doctor;
import com.ecarezone.domain.Patient;


public class CallRegistrationScheduler {

	@Autowired
	PatientDAO patientDAO;
	
	@Autowired
	DoctorDAO doctorDAO;

	@SuppressWarnings("deprecation")
	public void registerForCallService() {
		System.out.println("inside registerForCallService");

		List<Patient> patientsList = patientDAO.getPatientsForCallRegistration();
		
		System.out.println("patientsList size: "+patientsList.size());
		
		List<Doctor> doctorsList = doctorDAO.getDoctorsForCallRegistration();
		
		System.out.println("doctorsList size: "+doctorsList.size());
		
		//call();
		
		//registerPatientForCallService(patientsList.get(patientsList.size()-1));
		
//		registerDoctorForCallService(doctorsList.get(doctorsList.size()-1));
		
		if(!patientsList.isEmpty()){
			for (Patient patient : patientsList) {
				sinchRegisterJspCall(patient.getEmail(),patient.getHashedPassword(),patient.getEmail());
				registerPatientForCallService(patient);
				/*try {
				    Thread.sleep(9000);                 //1000 milliseconds is one second.
				} catch(InterruptedException ex) {
				    Thread.currentThread().interrupt();
				}*/
			}
		}
		
		//registerDoctorForCallService(doctorsList.get(doctorsList.size()-1));
		
		if(!doctorsList.isEmpty()){
			for (Doctor doctor : doctorsList) {
				sinchRegisterJspCall(doctor.getEmail(),doctor.getHashedPassword(),doctor.getEmail());
				registerDoctorForCallService(doctor);
			}
		}
		
	}
	
	private void registerPatientForCallService(Patient patient){
		System.out.println("inside registerPatientForCallService");
		patientDAO.registerPatientForCall(patient);
	}
	
	private void registerDoctorForCallService(Doctor doctor){
		doctorDAO.registerDoctorForCall(doctor);
	}
	
	public void sinchRegisterJspCall(String userName, String password, String email) {
		
		HttpURLConnection connection = null;
        
        try {
             String message = URLEncoder.encode("ABC", "UTF-8");
             
             URL url = new URL("http://localhost:8080/ECZ/pages/sinchRegister.jsp?userName="+userName+"&&password="+password+"&&email="+email);
             //System.out.println("Test.java: redirect(): url=" + url);
             
             connection = (HttpURLConnection) url.openConnection();
             //System.out.println("Test.java: redirect(): connection=" + connection);
             
             connection.setDoOutput(true);
             //System.out.println("Test.java: redirect(): after connection.setDoOutput()");
             
             connection.setRequestMethod("POST");
             //System.out.println("Test.java: redirect(): after connection.setRequestMethod()");
             
             OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
             //System.out.println("Test.java: redirect(): writer=" + writer);
             
             writer.write("message=" + message);
            // System.out.println("Test.java: redirect(): after writer.write()");
             
             writer.flush();
            // System.out.println("Test.java: redirect(): after writer.flush()");
             
             writer.close();
            // System.out.println("Test.java: redirect(): after writer.close()");

            // System.out.println("Test.java: redirect(): connection.getResponseCode()=" + connection.getResponseCode());
            // System.out.println("Test.java: redirect(): HttpURLConnection.HTTP_OK=" + HttpURLConnection.HTTP_OK);
             
             // if there is a response code AND that response code is 200 OK, do stuff in the first if block
           if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
               // OK
        	   System.out.println("Registration Done");
           // otherwise, if any other status code is returned, or no status code is returned, do stuff in the else block
           } else {
               // Server returned HTTP error code.
        	   System.out.println("Registration Failed");
           }
           
        } catch (UnsupportedEncodingException e) {
             e.printStackTrace();
             
        } catch (MalformedURLException e) {
             e.printStackTrace();
             
        } catch (IOException e) {
             e.printStackTrace();
             
        } finally {
             
             if (connection != null)
                  connection.disconnect();
        }
   }

	public static void main(String[] args) {
	//public void call()	{
	try {
		
		URL url = new URL("http://localhost:8080/ECZ/sinch/isalive");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream())); 
		String  line = in.readLine(); 

		System.out.println( line );	

		in.close(); 
		
		/*try{
		InputStreamReader isr = new InputStreamReader(System.in ) ; 
      	BufferedReader br = new BufferedReader(isr); 

      	String line = br.readLine() ; 

      	URL url = new URL( "http://localhost:8080/ECZ/pages/sinchRegister.jsp" ) ; 

      	URLConnection connection = url.openConnection() ; 

      	connection.setDoOutput(true) ; 

// The order in which input and output strams are acquired matters.	
      	ObjectOutputStream os = new ObjectOutputStream( connection.getOutputStream() ) ; 

os.writeObject( line ) ; 
os.close() ; 

      	ObjectInputStream is = new ObjectInputStream( connection.getInputStream() ) ; 
      	System.out.println( is.readObject() ) ; 
is.close() ; 
	} catch(Exception e){
		System.out.println("exception");
	}
	
	}*/
		
			
			/*HttpClient client = new DefaultHttpClient();
			  HttpGet post = new HttpGet("http://localhost:8080/ECZ/sinch/alive");
			  SinchUser user = new SinchUser();
			  post.setEntity(input);
			  HttpResponse response = client.execute(post);*/
			  /*BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			  String line = "";
			  while ((line = rd.readLine()) != null) {
			   System.out.println(line);
			  }*/
			
			/*DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(
				"http://localhost:8080/ECZ/sinch/signup");

			JSONObject json = new JSONObject();
			json.put("userName", "someValue"); 
			json.put("password", "somepassword"); 
			json.put("email", "someemail");
			
			StringEntity input = new StringEntity(json.toString());
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);*/

			/*if (response.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(
	                        new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}*/

			 // client.getConnectionManager().shutdown();

		  } catch (ClientProtocolException e) {
		
			e.printStackTrace();

		  } catch (IOException e) {
		
			e.printStackTrace();
		  }	catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
}
