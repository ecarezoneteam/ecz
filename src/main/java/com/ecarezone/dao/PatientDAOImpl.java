package com.ecarezone.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecarezone.domain.Doctor;
import com.ecarezone.domain.Patient;
import com.ecarezone.domain.XDoctorPatient;

@Repository("PatientDAO")
public class PatientDAOImpl extends AbstractDAO implements PatientDAO{

	/*@Resource(name = "sessionFactory")
	SessionFactory sessionFactory;*/

	@Transactional
	@Override
	public Patient getPatient(Long userId) {
		Criteria criteria = getSession().createCriteria(Patient.class).	 
				add(Restrictions.eq("userId", userId));
		Object result  =  criteria.list();
		List<Patient> patients = (List<Patient>) result;
		return patients.get(0);
	}

	@Transactional
	@Override
	public List<Patient> getPatientsForCallRegistration() {
		Criteria criteria = getSession().createCriteria(Patient.class).	 
							add(Restrictions.eq("isCallAllowed", "FALSE"));
		Object result  =  criteria.list();
		List<Patient> patients = (List<Patient>) result;
		return patients;
	}

	@Transactional
	@Override
	public void registerPatientForCall(Patient patient) {
		patient.setIsCallAllowed("TRUE");
		getSession().update(patient);
	}
	
	@Transactional
	@Override
	public List<XDoctorPatient> getRegisteredDoctors(Long patientId) {
		Criteria criteria = getSession().createCriteria(XDoctorPatient.class).	 
				add(Restrictions.eq("patientId", patientId));
		Object result  =  criteria.list();
		List<XDoctorPatient> list = (List<XDoctorPatient>) result;
		
		return list;
	}
	
	@Transactional
	@Override
	public void updatePatientStatus(Patient patient, String status) {
		patient.setStatus(status);
		getSession().update(patient);
		
	}


}
