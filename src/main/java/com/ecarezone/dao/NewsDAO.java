package com.ecarezone.dao;

import java.util.List;

import com.ecarezone.domain.News;

public interface NewsDAO {
	
	public List<News> fetchAllNews();
	
	public News getNews(Long id);
	
	public void updateNews(News news);
	
	public void deleteNews(News news);
	
	public boolean createNews(News news);
	
}
