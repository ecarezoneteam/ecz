package com.ecarezone.dao;

import java.util.List;

public interface NotificationRegisterDAO {
	
	public List<String> getPatientDeviceUnqiueKeys(String role);
	
	public String getUserDeviceUnqiueKey(String email);
	
	public void updateDeviceUniqueStatus(String deviceUnique, String status);
}
