package com.ecarezone.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecarezone.domain.News;

@Repository("NewsDAO")
public class NewsDAOImpl extends AbstractDAO implements NewsDAO{

	@Transactional
	@Override
	public List<News> fetchAllNews() {
		Criteria criteria = getSession().createCriteria(News.class);	 
		Object result  =  criteria.list();
		List<News> news = (List<News>) result;
		return news;
	}

	@Transactional
	@Override
	public News getNews(Long id) {
		Criteria criteria = getSession().createCriteria(News.class).	 
				add(Restrictions.eq("id", id));
		Object result  =  criteria.list();
		List<News> news = (List<News>) result;
		return news.get(0);
	}

	@Transactional
	@Override
	public void updateNews(News news) {
		getSession().saveOrUpdate(news);
	}

	@Transactional
	@Override
	public void deleteNews(News news) {
		getSession().delete(news);
	}

	@Transactional
	@Override
	public boolean createNews(News news) {
		try{
			getSession().save(news);
			return true;
		} catch (RuntimeException e) {
           return false;
        }
	}


}
