package com.ecarezone.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecarezone.domain.Doctor;
import com.ecarezone.domain.XDoctorPatient;

@Repository("DoctorDAO")
public class DoctorDAOImpl extends AbstractDAO implements DoctorDAO{

	/*@Resource(name = "sessionFactory")
	SessionFactory sessionFactory;*/

	@Transactional
	@Override
	public Doctor getDoctor(Long userId) {
		Criteria criteria = getSession().createCriteria(Doctor.class).	 
				add(Restrictions.eq("doctorId", userId));
	Object result  =  criteria.list();
	List<Doctor> doctors = (List<Doctor>) result;
	
		if(doctors.isEmpty()){
			return null;
		} else{
			return doctors.get(0);
		}
	}

	@Transactional
	@Override
	public List<Doctor> getDoctorsForCallRegistration() {
		Criteria criteria = getSession().createCriteria(Doctor.class).	 
							add(Restrictions.eq("isCallAllowed", "FALSE"));
		Object result  =  criteria.list();
		List<Doctor> doctors = (List<Doctor>) result;
		return doctors;
	}

	@Transactional
	@Override
	public void registerDoctorForCall(Doctor doctor) {
		doctor.setIsCallAllowed("TRUE");
		getSession().update(doctor);
		
	}

	@Transactional
	@Override
	public List<XDoctorPatient> getRegisteredPatients(Long doctorId) {
		Criteria criteria = getSession().createCriteria(XDoctorPatient.class).	 
				add(Restrictions.eq("doctorId", doctorId));
		Object result  =  criteria.list();
		List<XDoctorPatient> list = (List<XDoctorPatient>) result;
		
		return list;
	}

	@Transactional
	@Override
	public void updateDoctorStatus(Doctor doctor, String status) {
		doctor.setStatus(status);
		getSession().update(doctor);
		
	}

}
