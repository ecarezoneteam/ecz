package com.ecarezone.dao;

import java.util.List;

import com.ecarezone.domain.Doctor;
import com.ecarezone.domain.XDoctorPatient;

public interface DoctorDAO {
	
	public Doctor getDoctor(Long userId);
	
	public List<Doctor> getDoctorsForCallRegistration();
	
	public void registerDoctorForCall(Doctor doctor);
	
	public void updateDoctorStatus(Doctor doctor, String status);
	
	public List<XDoctorPatient> getRegisteredPatients(Long doctorId);

}
