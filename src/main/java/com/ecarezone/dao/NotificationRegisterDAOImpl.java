package com.ecarezone.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecarezone.domain.NotificationRegister;
import com.ecarezone.domain.XDoctorPatient;

@Repository("NotificationRegisterDAO")
public class NotificationRegisterDAOImpl extends AbstractDAO implements NotificationRegisterDAO{

	@Resource(name = "sessionFactory")
	SessionFactory sessionFactory;

	@Transactional
	@Override
	public List<String> getPatientDeviceUnqiueKeys(String role) {
		Criteria criteria = getSession().createCriteria(NotificationRegister.class); 
			
		Object result  =  criteria.setProjection(Projections.property("deviceUnique"))
								  .add(Restrictions.eq("role", role)).list();
		List<String> deviceUniqueKeys = (List<String>) result;
		return deviceUniqueKeys;
	}
	
	@Transactional
	@Override
	public String getUserDeviceUnqiueKey(String email) {
		Criteria criteria = getSession().createCriteria(NotificationRegister.class).
				add(Restrictions.eq("email", email)); 
		
			
		Object result  =  criteria.setProjection(Projections.property("deviceUnique")).list();
		List<String> deviceUniqueKeys = (List<String>) result;
		if(deviceUniqueKeys.size() >0){
		 return deviceUniqueKeys.get(0);
		} else{
			return null;
		}
	}
	
	@Transactional
	@Override
	public void updateDeviceUniqueStatus(String deviceUnique,String status) {
		Object result = getSession().createCriteria(NotificationRegister.class).add(Restrictions.eq("deviceUnique", deviceUnique)).list(); 
			
		List<NotificationRegister> notificationRegisterList = (List<NotificationRegister>) result;
		
		if(!notificationRegisterList.isEmpty()){
			NotificationRegister notificationRegister = notificationRegisterList.get(0);
			notificationRegister.setStatus(status);
			getSession().update(notificationRegister);
		}
	}

}
