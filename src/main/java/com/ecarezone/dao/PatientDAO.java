package com.ecarezone.dao;

import java.util.List;

import com.ecarezone.domain.Doctor;
import com.ecarezone.domain.Patient;
import com.ecarezone.domain.XDoctorPatient;

public interface PatientDAO {
	
	public Patient getPatient(Long userId);
	
	public List<Patient> getPatientsForCallRegistration();
	
	public void registerPatientForCall(Patient patient);

	public void updatePatientStatus(Patient doctor, String status);
	
	public List<XDoctorPatient> getRegisteredDoctors(Long patientId);
}
