package com.ecarezone.pojo;

public class PushStatus {

	private String name;
	private String email;
	private String status;
	private String deviceUnique;
	private String password;
	private String role;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDeviceUnique() {
		return deviceUnique;
	}
	public void setDeviceUnique(String deviceUnique) {
		this.deviceUnique = deviceUnique;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
