/**
 * 
 */
package com.ecarezone.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ecarezone.dao.NewsDAO;
import com.ecarezone.domain.News;
import com.ecarezone.service.NewsPushService;
import com.ecarezone.util.HashUtil;
import com.ecarezone.util.NewsCategoryUtil;

/**
 * @author Harish
 *
 */
@Controller("NewsController")
@RequestMapping("/news")
public class NewsController {
	
	
	@Autowired
	NewsDAO newsDAO;
	
	@Autowired
	NewsPushService newsPushService;
	
	@RequestMapping(value = "/createImage", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public ModelAndView createImage(@RequestParam("file") MultipartFile filedata, HttpServletRequest request,
			HttpServletResponse response) {
		
	/*	InputStream is = null;
		  try {
			is = filedata.getInputStream();
			
			//is.
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		 // byte[] bytes = IOUtils.toByteArray(is);
		
	//newsDAO.createNews(news);
		
		ModelAndView model = new ModelAndView("createNews");
		model.addObject("fileName", filedata.getOriginalFilename());
		
		return model;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public Object create(@RequestParam("file") MultipartFile filedata, @RequestParam("title") String title, @RequestParam("newsCategory") Long newsCategory,
			@RequestParam("newsAbstract") String newsAbstract, HttpServletRequest request,
			HttpServletResponse response) {
		
		String avatarUrl=null;
		
		String fileName = filedata.getOriginalFilename();
		File file = new File(fileName);
		try {
			filedata.transferTo(file);
			BufferedImage bImage=null;
            String uniqueAvatarId = HashUtil.uniqueAvatarId(fileName);
        	bImage = ImageIO.read(file);
        	ImageIO.write(bImage, "png",new File("/var/www/html/images/news/"+ uniqueAvatarId+".png") );
        	//ImageIO.write(bImage, "png", new File("C:\\Users\\LAP\\Desktop\\maven\\images\\"+ uniqueAvatarId+".png") );
        	avatarUrl = "http://188.166.55.204/images/news/"+uniqueAvatarId+".png";
        	
        	News newsPojo = new News();
        	newsPojo.setLink(avatarUrl);
        	newsPojo.setNewsAbstract(newsAbstract);
        	newsPojo.setTitle(title);
        	newsPojo.setNewsCategoryId(newsCategory);
        	
        	if(newsDAO.createNews(newsPojo)){ // if news creation is successful, only then push it
        		String notificationMessage = "News,"+newsAbstract;
        		newsPushService.pushNewsToPatientDevices(notificationMessage);
        	}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ModelAndView model = new ModelAndView("../pages/createNews");
		
		return model;
	}
	
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	@ResponseBody
	public Object modify(@RequestParam("id") String id, @RequestParam("title") String title, 
			@RequestParam("newsAbstract") String newsAbstract, HttpServletRequest request,
			HttpServletResponse response) {
		
		Long newId = Long.parseLong(id);
		News news = newsDAO.getNews(newId);
		
		news.setNewsAbstract(newsAbstract);
		news.setTitle(title);
		
		newsDAO.updateNews(news);
		
    	ModelAndView model = new ModelAndView("../pages/news");
		
		return model;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Object delete(@RequestParam("id") String id, HttpServletRequest request,
			HttpServletResponse response) {
		
		Long newId = Long.parseLong(id);
		News news = newsDAO.getNews(newId);
		
		newsDAO.deleteNews(news);
		
    	ModelAndView model = new ModelAndView("../pages/news");
		
		return model;
	}
	
	
	@RequestMapping(value = "/fetchModify", method = RequestMethod.POST)
	@ResponseBody
	public Object getNewsforModify(@RequestParam("id") String id, HttpServletRequest request,
			HttpServletResponse response) {
		
		
		Long newId = Long.parseLong(id);
		News news = newsDAO.getNews(newId);
		
		ModelAndView model = new ModelAndView("../pages/updateNews");
		model.addObject("title", news.getTitle());
		model.addObject("newsAbstract", news.getNewsAbstract());
		model.addObject("id", news.getId());
		model.addObject("newsCategory", news.getNewsCategoryId());
		
		return model;
	}
	
	
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	@ResponseBody
	public Object getAllNews(HttpServletRequest request,
			HttpServletResponse response) {
		
	List<News> newsList = newsDAO.fetchAllNews();
	
	JSONArray resultArray = new JSONArray();
	
	JSONObject newsJson = null;
	
	try{
		for (News news : newsList) {
			newsJson = new JSONObject();
			
			newsJson.put("id",news.id);
			newsJson.put("title",news.title);
			//System.out.println("news.newsCategoryId: "+news.newsCategoryId);
			if(news.newsCategoryId != null || news.newsCategoryId >0){
				newsJson.put("newsCategory",NewsCategoryUtil.getValue(news.newsCategoryId+""));
			}
			newsJson.put("newsAbstract",news.newsAbstract);
			newsJson.put("image",news.link);
			resultArray.add(newsJson);
		}
		
		String resultString = resultArray.toJSONString();
	
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(resultString);
		
	} catch(Exception e) {
		e.printStackTrace();
		response.setStatus(500);
	}
		
	return null;
	}
	
}
