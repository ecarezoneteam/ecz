/**
 * 
 */
package com.ecarezone.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecarezone.dao.DoctorDAO;
import com.ecarezone.dao.NotificationRegisterDAO;
import com.ecarezone.dao.PatientDAO;
import com.ecarezone.domain.Doctor;
import com.ecarezone.domain.Patient;
import com.ecarezone.domain.XDoctorPatient;
import com.ecarezone.pojo.PushStatus;
import com.ecarezone.service.NotificationService;

/**
 * @author Harish
 *
 */
@Controller("PushStatusController")
@RequestMapping("/notification")
public class PushStatusController {
	
	@Autowired
	DoctorDAO doctorDAO;
	
	@Autowired
	PatientDAO patientDAO;
	
	@Autowired
	NotificationRegisterDAO notificationRegisterDAO;
	
	String appKey = "IFWmeJ4dR_Ot8gUl0s0SdQ";
	String appMasterSecret = "mEX5kUmsSMS6wkOxS_Cy3w";
	
	@RequestMapping(value = "/pushstatus/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object pushStatus(@PathVariable Long id,@RequestBody PushStatus pushStatus, HttpServletRequest request,
			HttpServletResponse response) {
		
		String role = pushStatus.getRole();
		
		if(role == null){
			return "Role type is required";
		}
		
		switch (role) {
    	case "0":
    		return pushDoctorStaus(id, pushStatus); 
    	case "1":
    		return pushPatientStaus(id, pushStatus); 
    	default:
    		return "Role type should be 0 or 1";
		}
	}
	
	public String pushDoctorStaus(Long doctorId, PushStatus pushStatus){
		String docStatus = pushStatus.getStatus();
		String responseStatus = "Notification Sent";
		if(pushStatus != null &&(docStatus.equalsIgnoreCase("0")
				|| docStatus.equalsIgnoreCase("1")
					|| docStatus.equalsIgnoreCase("2"))){
			
			List<String> deviceUniqueKeys = new ArrayList<String>();
			
			Doctor doctor = doctorDAO.getDoctor(doctorId);
			if(doctor == null){
				return "No Doctor available with id: "+doctorId;
			} else if(!doctor.getEmail().equalsIgnoreCase(pushStatus.getEmail()) ||  !doctor.getHashedPassword().equalsIgnoreCase(pushStatus.getPassword())){
				return "UserName or Password is wrong";
			} else {
				doctorDAO.updateDoctorStatus(doctor, docStatus);
			}
			
			notificationRegisterDAO.updateDeviceUniqueStatus(pushStatus.getDeviceUnique(), docStatus);
			
			List<XDoctorPatient> doctorPatientList = doctorDAO.getRegisteredPatients(doctorId);
			if(doctorPatientList.isEmpty()){
				//no push notification if doctor is not available or no patients registered
				//System.out.println(doctorId+" is not available or no patients registered");
				responseStatus = "Docor with id: "+doctorId+" is not available or no patients registered";
				return responseStatus;
			}
			
			for (XDoctorPatient xDoctorPatient : doctorPatientList) {
				String email = patientDAO.getPatient(xDoctorPatient.getPatientId()).getEmail();
				System.out.println("email: "+email);
				String deviceUniqueKey = notificationRegisterDAO.getUserDeviceUnqiueKey(email);
				
				if(deviceUniqueKey != null){
					deviceUniqueKeys.add(deviceUniqueKey);
				}
			}
			
			String notification = "Doctor, "+doctorId+","+pushStatus.getStatus();
			if(!deviceUniqueKeys.isEmpty()){
				NotificationService.sendPush(appKey, appMasterSecret, deviceUniqueKeys, notification);
			}
			return responseStatus;
		} else{
			//no push notification for other status
			responseStatus = pushStatus.getStatus()+" Status is not available";
			return responseStatus;
		}
	}
	
	public String pushPatientStaus(Long id, PushStatus pushStatus){
		String patientStatus = pushStatus.getStatus();
		String responseStatus = "Notification Sent";
		if(pushStatus != null &&(patientStatus.equalsIgnoreCase("0")
				|| patientStatus.equalsIgnoreCase("1")
					|| patientStatus.equalsIgnoreCase("2"))){
			
			List<String> deviceUniqueKeys = new ArrayList<String>();
			
			Patient patient = patientDAO.getPatient(id);
			if(patient == null){
				return "No patient available with id: "+id;
			} else if(!patient.getEmail().equalsIgnoreCase(pushStatus.getEmail()) ||  !patient.getHashedPassword().equalsIgnoreCase(pushStatus.getPassword())){
				return "UserName or Password is wrong";
			} else {
				patientDAO.updatePatientStatus(patient, patientStatus);
			}
			
			notificationRegisterDAO.updateDeviceUniqueStatus(pushStatus.getDeviceUnique(), patientStatus);
			
			List<XDoctorPatient> doctorPatientList = patientDAO.getRegisteredDoctors(id);
			if(doctorPatientList.isEmpty()){
				//no push notification if doctor is not available or no doctors registered
				responseStatus = "Patient with id: "+id+" is not available or no doctors registered";
				return responseStatus;
			}
			
			for (XDoctorPatient xDoctorPatient : doctorPatientList) {
				String email = doctorDAO.getDoctor(xDoctorPatient.getDoctorId()).getEmail();
				System.out.println("email: "+email);
				String deviceUniqueKey = notificationRegisterDAO.getUserDeviceUnqiueKey(email);
				
				if(deviceUniqueKey != null){
					deviceUniqueKeys.add(deviceUniqueKey);
				}
			}
			
			String notification = "Patient, "+id+","+pushStatus.getStatus();
			if(!deviceUniqueKeys.isEmpty()){
				NotificationService.sendPush(appKey, appMasterSecret, deviceUniqueKeys, notification);
			}
			
			return responseStatus;
		} else{
			//no push notification for other status
			responseStatus = pushStatus.getStatus()+" Status is not available";
			return responseStatus;
		}
		
	}
	
}
