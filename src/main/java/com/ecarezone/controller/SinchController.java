/**
 * 
 */
package com.ecarezone.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ecarezone.pojo.SinchUser;

/**
 * @author Harish
 *
 */
@Controller("SinchController")
@RequestMapping("/sinch")
public class SinchController {
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ModelAndView signup(@RequestBody SinchUser user, HttpServletRequest request,
			HttpServletResponse response) {

		//appLogger.info("login successful for user: " + request.getParameter("username"));
		//System.out.println("username: " + request.getParameter("username"));
		
		System.out.println("inside signup");
		ModelAndView model = new ModelAndView();
		model.addObject("userName", user.getUserName());
		model.addObject("password", user.getPassword());
		model.addObject("email", user.getEmail());
		model.setViewName("sinchRegister");
		
		System.out.println("returning view");
		return model;
		
		/*System.out.println("usernmae: "+user.getUserName()+user.getPassword());
		
		return null;*/

	}
	
	@RequestMapping(value = "/signuptest", method = RequestMethod.GET)
	public ModelAndView signupTest(@RequestBody SinchUser user, HttpServletRequest request,
			HttpServletResponse response) {

		//appLogger.info("login successful for user: " + request.getParameter("username"));
		//System.out.println("username: " + request.getParameter("username"));
		
		System.out.println("inside signup");
		ModelAndView model = new ModelAndView();
		model.addObject("userName", "userName");
		model.addObject("password", "password");
		model.addObject("email", "email");
		model.setViewName("sinchRegister");
		
		System.out.println("returning view");
		return model;
		
		/*System.out.println("usernmae: "+user.getUserName()+user.getPassword());
		
		return null;*/

	}
	
	@RequestMapping(value = "/isalive", method = RequestMethod.GET)
	public ModelAndView isAlive(HttpServletRequest request,
			HttpServletResponse response) {

		//appLogger.info("login successful for user: " + request.getParameter("username"));
		//System.out.println("username: " + request.getParameter("username"));
		
		System.out.println("inside isalive");
		/*ModelAndView model = new ModelAndView();
		model.setViewName("sinchRegister");
		return model;*/
		
		try {
			response.sendRedirect("http://localhost:8080/ECZ/pages/sinchRegister.jsp");
			//response.sendRedirect("http://localhost:8080/ECZ/pages/sinchRegister.jsp?userName="++"password="++"email="+);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		return null;

	}
	
	
	@RequestMapping(value = "/updateDB", method = RequestMethod.GET)
	public ModelAndView udpateDB(HttpServletRequest request,
			HttpServletResponse response) {

		//appLogger.info("login successful for user: " + request.getParameter("username"));
		//System.out.println("username: " + request.getParameter("username"));
		
		System.out.println("inside udpatedb");

		return null;
	}


}
