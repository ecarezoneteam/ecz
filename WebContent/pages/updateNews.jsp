<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<%@ page import="java.io.*,java.text.*,java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Details</title>
<c:set var="root" value="${pageContext.request.contextPath}" />
<link rel="stylesheet" href="${root}/css/bootstrap.min.css">
<link rel="stylesheet" href="${root}/css/custom.css">
<script type="text/javascript" src="${root}/js/jquery-2.1.3.js"></script>
<script type="text/javascript" src="${root}/js/jquery.ajaxfileupload.js"></script>
<script type="text/javascript" src="${root}/js/jquery.form.js"></script>
<script src="${root}/js/bootstrap.min.js"></script>
<style>
.highlight {
	background-color: yellow;
}

.cart {
	overflow: hidden;
	padding: 10px 3px;
}
</style>
</head>
<body>
	<script>
	
     </script>

	<%-- <form id="newsform" name="newsform" method="post" enctype="multipart/form-data"  action="${root}/news/createImage">
		
		<div class="bodyContainer">
			
			<div style="height:350px;overflow-y:auto;overflow-x:hidden; margin-left:15%;width:85%;padding-top: 10px;">
			<h2>Create Image</h2>
				
			
			
				 <input id='imageId'  class="form-control" type="file" name="file" accept="image/*">
				 <button type="submit" class="btn btn-success btn-sm">Upload</button>
			</div>
		</div>
		
	</form> --%>
	
	<div class="bodyContainer" style="margin-bottom:-70px">
	    <div class="row">
	        <div class="col-md-4 col-md-offset-7">
	            <div class="panel panel-default">
	              <br><br>
	                <div class="panel-heading"> <strong class="">Create News</strong>
	
	                </div>
	                <div class="panel-body">
	                    <form id="newsform" name="newsform" class="form-horizontal" method="post" action="${root}/news/modify" >
	                       
	                        <div class="form-group">
	                            <label for="inputUsername3" class="col-sm-3 control-label">Id</label>
	                           <div class="col-sm-9">
	                            	 <input id='newId' class="form-control" type="text" name="newId" value="${id}" readonly="readonly" />
	                            </div>
	                        </div>
	                       
	                        <div class="form-group">
	                            <label for="inputUsername3" class="col-sm-3 control-label">News Category</label>
	                            <div class="col-sm-9">
	                            	 <input id='newsCategoryTextBoxId' class="form-control" type="text" name="newsCategory"  value="${newsCategory}" readonly="readonly" />
	                            </div>
	                            
	                        </div>
	                        <div class="form-group">
	                            <label for="inputUsername3" class="col-sm-3 control-label">Title</label>
	                            <div class="col-sm-9">
	                            	 <input id='titleId' class="form-control" type="text" placeholder="Title" name="title"  value="${title}" required  />
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="inputUsername3" class="col-sm-3 control-label">News Abstract</label>
	                            <div class="col-sm-9">
	                            	 <input id='newsAbstractId' class="form-control" type="text" placeholder="News Abstract" name="newsAbstract" value="${newsAbstract}" required  />
	                            </div>
	                        </div>
	                      
	                        <div class="form-group last">
	                            <div class="col-sm-offset-3 col-sm-9">
	                                <button type="submit" class="btn btn-success btn-sm">Update</button>
	                            </div>
	                        </div>
	                        
	                        <div class="custom-backbutton" style="margin-left:20%">
								<a type="button" class="btn btn-success btn-sm"
									href="${root}/pages/news.jsp">Back</a>
							</div>
	                    </form>
	                </div>
	            </div>
	        </div>
		</div>
	</div> 
	
	
</body>
</html>