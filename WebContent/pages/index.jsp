<%-- 
    Document   : HomePage
    Created on : May 26, 2015, 4:20:15 PM
    Author     : coxadmin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %> --%>
 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
  		<link rel="stylesheet" href="${root}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${root}/css/custom.css">
        <script type="text/javascript" src="${root}/js/jquery-2.1.3.js"></script>
        <script src="${root}/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function ()
            {

                // disable back button
                window.location.hash = "no-back-button";
                window.location.hash = "Again-No-back-button"; //again because google chrome don't insert first hash into history
                window.onhashchange = function () {
                    window.location.hash = "no-back-button";
                }

                // F5 and Ctrl+R disable
                $(document).on("keydown", keydown);
                $(document).on("keyup", keyup);
                // disable right click
                $(document).on({
                    "contextmenu": function (e) {
                        console.log("ctx menu button:", e.which);
                        // Stop the context menu
                        e.preventDefault();
                    },
                    "mousedown": function (e) {
                        console.log("normal mouse down:", e.which);
                    },
                    "mouseup": function (e) {
                        console.log("normal mouse up:", e.which);
                    }
                });
                // testCaseExecutionButton
                $('#testCaseExecutionId').click(function ()
                {
                    $('#PSIA_ACTION_ID').val('TESTCASESELECTION');
                });
            });
            function keydown(e) {

                if ((e.which || e.keyCode) == 116 || ((e.which || e.keyCode) == 82 && ctrlKeyDown)) {
                    // Pressing F5 or Ctrl+R
                    alert(' Refresh Ignored Here ');
                    e.preventDefault();
                } else if ((e.which || e.keyCode) == 17) {
                    // Pressing  only Ctrl
                    ctrlKeyDown = true;
                }
            }


            function keyup(e) {
                // Key up Ctrl
                if ((e.which || e.keyCode) == 17)
                    ctrlKeyDown = false;
            }
        </script>
    </head>
    <body>
    
     <div class="bodyContainer" >
      
      <div style="text-align: justify;margin-left:20%;">
     <B> <br>Welcome to ECareZone portal !!<br></B>
      <br>This portal will perform few tasks like<br>
      <br>1. Register all the users (doctor and patient) to Sinch<br>
      <br>2. Register all the users (doctor and patient) to Urban Airship<br>
      <br>3. Send Push Notifications on latest News<br>
      <br>4. Send Push Notifications on doctors status (online/offline/idle)<br>
      </div>
      <br>
      <!-- <h3 style="color:#CC9966; text-align: center;">Please Select any of the Tabs from the Left Panel</h3> -->
     </div>
    
    
        


    
        
    </body>
</html>
