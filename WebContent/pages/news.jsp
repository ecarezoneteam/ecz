<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<%@ page import="java.io.*,java.text.*,java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News Details</title>
<c:set var="root" value="${pageContext.request.contextPath}" />
<link rel="stylesheet" href="${root}/css/bootstrap.min.css">
<link rel="stylesheet" href="${root}/css/custom.css">
<script type="text/javascript" src="${root}/js/jquery-2.1.3.js"></script>
<script src="${root}/js/bootstrap.min.js"></script>
<style>
.highlight {
	background-color: yellow;
}

.cart {
	overflow: hidden;
	padding: 10px 3px;
}
</style>
</head>
<body>
	<script>
     $(document).ready(function ()
             {    	
     
    	 fetchAllNews();   
    	
    	
       });  
     
     function fetchAllNews()     
     {
    	
         $.get("${root}/news/fetchAll",
                 function (data, status) {
                     if (status == "success")
                     {
                         $("#newsTableData").empty();
                         $.each(data, function (idx, obj) {
                        	 $("#newsTableData").append("<tr  class='testCasedata' id=' " + obj.id + "' ><td style='word-wrap: break-word;text-align:center;height:50px'>" + obj.id + "</td><td style='word-wrap: break-word;text-align:center;height:50px'>" + obj.newsCategory + "</td><td style='word-wrap: break-word;text-align:center;height:50px'>" + obj.title + "</td><td style='word-wrap: break-word;text-align:center;height:50px'>" + obj.newsAbstract + "</td><td style='word-wrap: break-word;text-align:center;'>" + obj.image + "</td><td style='word-wrap: break-word;text-align:center;'> <a href = '#' id='deleteLink'onclick='deleteNews("+obj.id+")'>" + "Delete" + "</td></tr>");
                         });  
                     }
                     else
                     {
                         alert('failure in fetching News');
                     }
                 });
     }
     
     
     function modifyNews(id)   //fetch news  
     { 
    	 alert("modify");
    	 $.post("${root}/news/fetchModify",
			 {
		 		id : id
			 },
             function (data, status) {
                 if (status == "success")
                 {
                	 
                 }
                 else
                 {
                	 alert("failure in fetching news for modify")
                 }
             });
     }
     
     function deleteNews(Id)     
     { 
    	 
    	 if (confirm("Are you sure?")) {
    		 $.post("${root}/news/delete",
    				 {
    					 id : Id,
    				 },
    	             function (data, status) {
    	                 if (status == "success")
    	                 {
    	                	 fetchAllNews();
    	                 }
    	                 else
    	                 {
    	                	 alert("failure in delete news")
    	                 }
    	             });
    	    }
    	    return false;
     }
     
     function createNews()     
     {
    	alert("hello");
    	// window.location="http://localhost:8080/ECZ/pages/createNews.jsp";
     }
     
     </script>

	<form id="myform" name="myform" method="post">
		
		<div class="bodyContainer">
			
			<div style="height:750px;overflow-y:auto;overflow-x:hidden; margin-left:2%;width:97%;padding-top: 10px;">
			
			<br><br>
			<h2 style="margin-left:40%">News Details</h2>
			
			<button style="text-align: center;margin-left:90%;width:10%;" type="reset" onclick="location.href='${root}/pages/createNews.jsp'">
   				Create News
			</button>
			
			<table id="newsTable"
				class="table table-striped table-bordered table-hover" 
				style="width: 120%; height: 0px; font-size:11px;text-align: left;">
				<thead>
					<tr class="success">
						<th>Id</th>
						<th>News Category</th>
						<th>Title</th>
						<th>News Abstract</th>
						<th>Image</th>
						<!-- <th>Edit</th> -->
						<th>Delete</th>
					</tr>
				</thead>
				
				<tbody id='newsTableData' class="text-left">
				 <tr>
					    <td >
					      <div>
					        Loading.............................................
					      </div>
					    </td>
					  </tr>
				</tbody>
				
			</table>
			</div>
<br>
		</div>
		
	</form>
</body>
</html>